(function() {
    'use strict';

    /* globals scUtils, passage, Dialog, jQuery */

    class AchievementRenderer {
        constructor() {
            const unlocked = JSON.parse(localStorage.getItem('unlocked') || '[]');
            window.pillared.achievements.forEach((achievement) => {
                if (unlocked.includes(achievement.id)) {
                    achievement.unlocked = true;
                }
            });

            this.manager = new window.pillared.AchievementManager(window.pillared.achievements, this.onUnlock.bind(this));

            jQuery(document).on(':passagedisplay', this.onPassageDisplay.bind(this));

            this.$notificationContainer = jQuery('<div class="achievements-container"></div>');
            this.$notificationContainer.appendTo('body');
            this.$notificationContainer.on('click', () => {
                this.displayAchievementsList();
            });

            scUtils.createHandlerButton('Достижения', '\\e809\\00a0', 'achievements', () => {
                this.displayAchievementsList();
            });

            this._icon = Story.get('icon-achievement').processText()
        }

        onUnlock(achievements) {
            const {unlocked} = this.manager.getOverview();
            const unlockedIds = unlocked.map((a) => a.id);
            localStorage.setItem('unlocked', JSON.stringify(unlockedIds));

            this.displayNotification(achievements);
        }

        displayNotification(achievements) {
            this.$notificationContainer.html(
                achievements.map(this.renderAchievement, this)
            );
            this.$notificationContainer.addClass('open');

            setTimeout(this.hideNotification.bind(this), 5000);
        }

        hideNotification() {
            this.$notificationContainer.removeClass('open');
            this.$notificationContainer.one('animationend', () => {
                this.$notificationContainer.html('')
            });
        }

        displayAchievementsList() {
            const overview = this.manager.getOverview();

            let html = overview.unlocked.map(this.renderAchievement, this).join('');

            if (overview.hidden > 0) {
                const hiddenAchievements = window.scUtils.pluralize(overview.hidden, ['скрытое достижение', 'скрытых достижения', 'скрытых достижений']);
                html += `
                    <p>${overview.unlocked.length > 0 ? 'И еще ' : ''}${overview.hidden} ${hiddenAchievements}.</p>
                `
            }

            Dialog.setup('Достижения', 'achievement-dialog');
            Dialog.append(html);
            Dialog.open();
        }

        onPassageDisplay() {
            this.manager.test();
        }

        renderAchievement(achievement) {
            return `
                <div class="achievement">
                    ${this._icon}
                    <div class="achievement-content">
                        <h6 class="achievement-title">${achievement.title}</h6>
                        <p class="achievement-text">${achievement.description}</p>	
                    </div>
                </div>
            `
        }
    }

    window.pillared = Object.assign(window.pillared || {}, {
        AchievementRenderer,
    });
}());