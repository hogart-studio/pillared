(function () {
    'use strict';

    const achievements = [
        {
            id: 'blood-on-the-sand',
            title: 'Blood on Sand',
            description: 'Die in the shoot-out',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (['Бедуин-2-стрелять', 'Бедуин-3-ждать'].includes(currentPassage)) {
                    return true;
                }
            }
        },
        {
            id: 'price-of-knowledge',
            title: 'Price of Knowledge',
            description: 'Learn the history of Irem',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (currentPassage === 'Лондон-Армагеддон-4') {
                    return true;
                }
            }
        },
        {
            id: 'price-of-life',
            title: 'Price of Life',
            description: 'Sacrifice yourself',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (currentPassage === 'Револьвер') {
                    return true;
                }
            }
        },
        {
            id: 'limelight',
            title: 'Limelight',
            description: 'Get recognized by the scientific society',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (currentPassage === 'Лондон-Богач-Сенсация') {
                    return true;
                }
            }
        },
        {
            id: 'finis',
            title: 'Finis',
            description: 'Finish the Game',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (['Лондон-Богач-Безумец', 'Лондон-Бедняк-Безумец', 'Лондон-Бедняк-Умеренно'].includes(currentPassage)) {
                    return true;
                }
            }
        },
        {
            id: 'drown-in-well',
            title: 'Like a Stone into Water',
            description: 'Drown in the well',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (currentPassage === 'Город-Колодец-Перегнуться') {
                    return true;
                }
            }
        },
    ];

    window.pillared = Object.assign(window.pillared || {}, {
        achievements,
    });
}());