(function () {
    'use strict';

    const achievements = [
        {
            id: 'blood-on-the-sand',
            title: 'Кровь на песке',
            description: 'Умереть в перестрелке с бедуином',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (['Бедуин-2-стрелять', 'Бедуин-3-ждать'].includes(currentPassage)) {
                    return true;
                }
            }
        },
        {
            id: 'price-of-knowledge',
            title: 'Цена знания',
            description: 'Привезти тварь в Лондон и узнать историю Ирема',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (currentPassage === 'Лондон-Армагеддон-4') {
                    return true;
                }
            }
        },
        {
            id: 'price-of-life',
            title: 'Цена жизни',
            description: 'Пожертвовать собой',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (currentPassage === 'Револьвер') {
                    return true;
                }
            }
        },
        {
            id: 'limelight',
            title: 'В лучах славы',
            description: 'Получить признание научного сообщества',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (currentPassage === 'Лондон-Богач-Сенсация') {
                    return true;
                }
            }
        },
        {
            id: 'finis',
            title: 'Finis',
            description: 'Пройти игру',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (['Лондон-Богач-Безумец', 'Лондон-Бедняк-Безумец', 'Лондон-Бедняк-Умеренно'].includes(currentPassage)) {
                    return true;
                }
            }
        },
        {
            id: 'drown-in-well',
            title: 'Как камень в воду',
            description: 'Утонуть в колодце',
            unlocked: false,
            hidden: true,
            test() {
                const currentPassage = passage();
                if (currentPassage === 'Город-Колодец-Перегнуться') {
                    return true;
                }
            }
        },
    ];

    window.pillared = Object.assign(window.pillared || {}, {
        achievements,
    });
}());