function preloadImage(source, strict = false) {
    return new Promise((resolve, reject) => {
        const img = new Image();
        img.onload = () => {
            resolve(img);
        };
        if (strict) {
            img.onerror = reject;
        } else {
            img.onerror = () => {
                resolve(null);
            }
        }

        img.src = source;
    });
}
function preloadImages(sources, strict) {
    return Promise.all(
        sources.map(
            (src) => preloadImage(src, strict)
        )
    );
}