#!/bin/sh

BUILD=`cat build`
BUILD=`expr $BUILD + 1`
echo $BUILD > build
VERSION=`cat pillared-deb/usr/share/pillared/package.json | jq -r '.version'`-$BUILD

echo Version: $VERSION

cd pillared-deb

echo $(pwd)

chmod -R 755 usr
find usr -type f -exec chmod 644 {} \;
chmod 755 usr/share/pillared/pillared
chmod 755 usr/games/pillared


INSTALLED_SIZE=`du -sk usr/ | awk '{print $1}'`

sed -i 's/^Version:.*/Version: '$VERSION'/' DEBIAN/control
sed -i 's/^Installed-Size:.*/Installed-Size: '$INSTALLED_SIZE'/' DEBIAN/control
md5deep -lr usr > DEBIAN/md5sums

cd ..

echo $(pwd)

fakeroot dpkg-deb --build pillared-deb pillared-$VERSION.deb

lintian pillared-$VERSION.deb

exit 0