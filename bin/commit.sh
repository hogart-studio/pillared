#!/bin/bash
for filename in irem*.html; do
	escapedfilePath=$(printf '%q' "$filename")
	cp "$filename" index.html
	fileDate=`stat -c %y -- "$filename"`
	echo $fileDate
	git add index.html
	git commit --date="$fileDate" --message="$filename" index.html
done